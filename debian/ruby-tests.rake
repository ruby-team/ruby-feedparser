require 'gem2deb/rake/testtask'

exclude = %w[
  test/tc_htmloutput.rb
  test/tc_textoutput.rb
  test/tc_parser.rb
  test/tc_textwrappedoutput.rb
]

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/tc_*.rb'] - exclude
end
